﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class EFRepository<TEntity> : IRepository<TEntity> where TEntity:class 
    {


        protected DbContext RepositoryDbContext { get; set; }
        protected DbSet<TEntity> RepositoryDbSet { get; set; }

        public EFRepository(IDataContext dataContext)
        {
            RepositoryDbContext = dataContext as DbContext ?? throw new ArgumentNullException(nameof(dataContext));
            RepositoryDbSet = RepositoryDbContext.Set<TEntity>() ??
                              throw new NullReferenceException($"Congratulations You broke it! -> DbSet for {nameof(TEntity)} not found");
        }

        public virtual IEnumerable<TEntity> All() => RepositoryDbSet.ToList();

    }
}
