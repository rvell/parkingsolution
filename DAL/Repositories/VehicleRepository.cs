﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain;
using Interfaces;


namespace DAL.Repositories
{
     public class VehicleRepository:EFRepository<Vehicle>, IVehicleRepository

    {
        public VehicleRepository(IDataContext dataContext) : base(dataContext)
        {
        }
    }


}
