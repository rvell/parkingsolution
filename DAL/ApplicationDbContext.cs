﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain;
using Interfaces;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class  ApplicationDbContext: IdentityDbContext<ApplicationUser>, IDataContext
    {
        public DbSet<Domain.Vehicle> Vehicles { get; set; } //TODO: Write Dbset for all classes!

        public ApplicationDbContext(DbContextOptions options):base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            
        }
    }
}
