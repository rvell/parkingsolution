﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> All();
    }
}
