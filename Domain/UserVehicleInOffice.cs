﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class UserVehicleInOffice
    {
        public int Id { get; set; }
        public bool Active { get; set; }


        public int UserVehicleId { get; set; }
        public UserVehicle UserVehicle { get; set; }

        public int OfficeId { get; set; }
        public Office Office { get; set; }

    }
}
