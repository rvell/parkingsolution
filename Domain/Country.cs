﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Country
    {
        
        public int CountryId { get; set; }
        public string Name { get; set; }
        public byte[] Flag { get; set; }
        public string NameShort { get; set; }
        public string PhoneCode { get; set; }

        public List<City> Cities { get; set; }
    }
}
