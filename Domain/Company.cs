﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Company
    {
        
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }

        public CompanyType CompanyType { get; set; }
        public int CompanyTypeId { get; set; }

        public ICollection<Office> Offices { get; set; }
    }
}