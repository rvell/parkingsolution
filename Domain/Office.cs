﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Office
    {
        public int OfficeId { get; set; }
        public string Name { get; set; }
        public byte[] Picture { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        
        public Company Company { get; set; }
        public int CompanyId { get; set; }

        public City City { get; set; }
        public int CityId { get; set; }

        public List<UserVehicleInOffice> UserVehicleInOffices { get; set; }
        public List<UserJoinedOffice> UserJoinedOffices { get; set; }
    }
}
