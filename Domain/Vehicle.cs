﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Vehicle
    {
        
        public int VehicleId { get; set; }
        public byte[] Picture { get; set; }
        public string Mark { get; set; }
        public string Model { get; set; }
        public string Plates { get; set; }
        public byte[] PlatesPicture { get; set; }
        public string Color { get; set; }

        public List<UserVehicle> UserVehicles { get; set; }
    }
}
