﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class UserVehicle
    {

        public int Id { get; set; }
        public bool Active { get; set; }


        public string UserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        

        public int VehicleId { get; set; }
        public Vehicle Vehicle { get; set; }

        public List<UserVehicleInOffice> UserVehicleInOffices { get; set; }

    }
}
