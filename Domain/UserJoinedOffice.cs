﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class UserJoinedOffice
    {
        public int Id { get; set; }
        public bool Active { get; set; }

        public int OfficeId { get; set; }
        public Office Office { get; set; }

        public string UserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public List<ApplicationUser> ApplicationUsers { get; set; }

    }
}
