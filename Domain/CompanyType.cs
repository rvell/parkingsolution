﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class CompanyType
    {
        
        public int CompanyTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public List<Company> Companies { get; set; }

    }
}
