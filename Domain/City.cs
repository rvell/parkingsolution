﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Domain
{
    public class City
    {
        
        public int CityId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Country Country { get; set; }
        public int CountryId { get; set; }

        public List<Office> Offices { get; set; }

    }
}
