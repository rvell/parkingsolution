﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Domain
{
    public class ApplicationUser: IdentityUser
    {

        [Required]
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte[] Photo { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string UserAddress { get; set; }

        public List<UserVehicle> UserVehicles { get; set; }
        public List<UserJoinedOffice> UserJoinedOffices { get; set; }
        
        
    }
}
